# -*- coding: utf-8 -*-
# https://towardsdatascience.com/sentence-correctness-classifier-using-transfer-learning-with-huggingface-bert-8884795ba5ca
#KEEP CONTROL FOR LABELS NUMBER

import os
import csv
import json
import numpy
from os import path

import pandas as pd
from scipy.special import softmax

datapath ='/kaggle/input/simpletransbotdata/'
best_model_path ='/kaggle/working/best_model'

os.mkdir(best_model_path)

data = []
data.append({"epoch":0,"avg_val_accuracy":0.0,"batch_size":0,"learning_rate":0.0})
with open(best_model_path +'/best_result.json', 'w') as outfile:
    json.dump(data, outfile)

# df = pd.read_csv("../input/cola-the-corpus-of-linguistic-acceptability/cola_public/raw/in_domain_train.tsv", delimiter='\t', header=None, names=['sentence_source', 'label', 'label_notes', 'sentence'])
df = pd.read_csv(datapath +'train.csv', error_bad_lines=False,skiprows=1,header=None,names=[ 'sentence','label'])

# train_df.columns = ['text', 'labels']

# Report the number of sentences.
print('Number of training sentences: {:,}\n'.format(df.shape[0]))

# Display 10 random rows from the data.
print(df.sample(20))

sentences = df.sentence.values
labels = df.label.values


from transformers import BertTokenizer

# tokenizer = BertTokenizer.from_pretrained('bert-base-uncased', do_lower_case=True)
tokenizer = BertTokenizer.from_pretrained('/kaggle/input/bertfinnishdata', do_lower_case=True)
# tokenizer = BertTokenizer.from_pretrained('TurkuNLP/bert-base-finnish-uncased-v1',do_lower_case=True)

max_len = 0

# For every sentence...
for sent in sentences:

    # Tokenize the text and add `[CLS]` and `[SEP]` tokens.
    input_ids = tokenizer.encode(sent, add_special_tokens=True)

    # Update the maximum sentence length.
    max_len = max(max_len, len(input_ids))

print('Max sentence length: ', max_len)


# os.system('pip install -U wandb')
os.system('wandb login c6fd4e313051ec1cd535f712eb55345d5182459f')
import wandb

# wandb.init(project="bertwandb")
# os.system('wandb agent remotejob/transformers/cjp37n3b')
sweep_config = {
    # 'project': 'bertwandb',
    'method': 'bayes', #grid, random bayes
    'metric': {
      'name': 'val_accuracy',
      'goal': 'maximize',
      'target': 0.87  
    },
    # 'parameters': {
    #     'learning_rate': {
    #         'values': [1e-4,1e-5,1e-6,5e-6,1e-6]
    #     },
    #     'batch_size': {
    #         'values': [32, 64,128,256]
    #     },
    #     'epochs':{
    #         'values':[300,400,500,550,600]
    #     }
    # },
        'parameters': {
        'learning_rate': {
            'values': [5e-6]
        },
        'batch_size': {
            'values': [32]
        },
        'epochs':{
            'values':[1000]
        }
    },
    'early_terminate': {
        'type': 'hyperband',
        # 's': 2,
        # 'eta': 3,
        # 'max_iter': 27
    }
}
# sweep_defaults = {
#         'learning_rate': 1e-5,       
#         'batch_size': 32,
#         'epochs':2
#         # 'best_val_accuracy': 0
# }


sweep_id = wandb.sweep(sweep_config)

import torch
# Tokenize all of the sentences and map the tokens to thier word IDs.
input_ids = []
attention_masks = []

# For every sentence...
for sent in sentences:
 
    encoded_dict = tokenizer.encode_plus(
                        sent,                      # Sentence to encode.
                        add_special_tokens = True, # Add '[CLS]' and '[SEP]'
                        max_length = 32,           # Pad & truncate all sentences. #64
                        pad_to_max_length = True,
                        return_attention_mask = True,   # Construct attn. masks.
                        return_tensors = 'pt',     # Return pytorch tensors.
                   )
    
    # Add the encoded sentence to the list.    
    input_ids.append(encoded_dict['input_ids'])
    
    # And its attention mask (simply differentiates padding from non-padding).
    attention_masks.append(encoded_dict['attention_mask'])

# Convert the lists into tensors.
input_ids = torch.cat(input_ids, dim=0)
attention_masks = torch.cat(attention_masks, dim=0)
labels = torch.tensor(labels)

# Print sentence 0, now as a list of IDs.
print('Original: ', sentences[0])
print('Token IDs:', input_ids[0])

from torch.utils.data import TensorDataset, random_split

# Combine the training inputs into a TensorDataset.
dataset = TensorDataset(input_ids, attention_masks, labels)

# Create a 90-10 train-validation split.

# Calculate the number of samples to include in each set.
train_size = int(0.9 * len(dataset))
val_size = len(dataset) - train_size

# Divide the dataset by randomly selecting samples.
train_dataset, val_dataset = random_split(dataset, [train_size, val_size])

print('{:>5,} training samples'.format(train_size))
print('{:>5,} validation samples'.format(val_size))

from torch.utils.data import DataLoader, RandomSampler, SequentialSampler
import wandb

def ret_dataloader():
    batch_size = wandb.config.batch_size
    # batch_size = 16 #32 #64
    print('batch_size dataloader = ', batch_size)
    train_dataloader = DataLoader(
                train_dataset,  # The training samples.
                sampler = RandomSampler(train_dataset), # Select batches randomly
                batch_size = batch_size # Trains with this batch size.
            )

    validation_dataloader = DataLoader(
                val_dataset, # The validation samples.
                sampler = SequentialSampler(val_dataset), # Pull out batches sequentially.
                batch_size = batch_size # Evaluate with this batch size.
            )
    return train_dataloader,validation_dataloader

from transformers import BertForSequenceClassification, AdamW, BertConfig

def ret_model():

    model = BertForSequenceClassification.from_pretrained(
        # "bert-base-uncased",
        "/kaggle/input/bertfinnishdata",
        # "TurkuNLP/bert-base-finnish-uncased-v1",
        num_labels = 68, 
        output_attentions = False, # Whether the model returns attentions weights.
        output_hidden_states = False, # Whether the model returns all hidden-states.
    )

    return model

def ret_optim(model):
    # print('Learning_rate = ',wandb.config.learning_rate )
    # print('Learning_rate = ',5e-5)
    optimizer = AdamW(model.parameters(),
                      lr = wandb.config.learning_rate, 
                      eps = 5e-9 
                    )
    return optimizer

from transformers import get_linear_schedule_with_warmup

def ret_scheduler(train_dataloader,optimizer):
    epochs = wandb.config.epochs
    # print('epochs =>', epochs)
    # Total number of training steps is [number of batches] x [number of epochs]. 
    # (Note that this is not the same as the number of training samples).
    total_steps = len(train_dataloader) * epochs

    # Create the learning rate scheduler.
    scheduler = get_linear_schedule_with_warmup(optimizer, 
                                                num_warmup_steps = 250, # Default value in run_glue.py #20
                                                num_training_steps = total_steps)
    return scheduler

import numpy as np

# Function to calculate the accuracy of our predictions vs labels
def flat_accuracy(preds, labels):
    pred_flat = np.argmax(preds, axis=1).flatten()
    labels_flat = labels.flatten()
    return np.sum(pred_flat == labels_flat) / len(labels_flat)
import time
import datetime

def format_time(elapsed): 
    elapsed_rounded = int(round((elapsed)))
    
    # Format as hh:mm:ss
    return str(datetime.timedelta(seconds=elapsed_rounded))

#torch.multiprocessing.set_start_method('spawn', force=True)

"""# The Train Function"""
import random
import numpy as np

def train():
    device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
    model = ret_model()
    model.to(device)
    # wandb.init(config=sweep_defaults)
    wandb.init()
    train_dataloader,validation_dataloader = ret_dataloader()
    optimizer = ret_optim(model)
    scheduler = ret_scheduler(train_dataloader,optimizer)
    seed_val = 42
   
    random.seed(seed_val)
    np.random.seed(seed_val)
    torch.manual_seed(seed_val)
    #torch.cuda.manual_seed_all(seed_val)

    training_stats = []

    # Measure the total training time for the whole run.
    total_t0 = time.time()
    # print('wandb.config.epochs',wandb.config.epochs)
    epochs = wandb.config.epochs
         
    for epoch_i in range(0, epochs):        

        print("")
        print('======== Epoch {:} / {:} ========'.format(epoch_i + 1, epochs))
        print('Training...')

        # Measure how long the training epoch takes.
        t0 = time.time()

        # Reset the total loss for this epoch.
        total_train_loss = 0.0

        model.train()

        # For each batch of training data...
        for step, batch in enumerate(train_dataloader):

            # Progress update every 40 batches.
            if step % 5 == 0 and not step == 0:
                # Calculate elapsed time in minutes.
                elapsed = format_time(time.time() - t0)
                # wandb.log({'lr':scheduler.get_lr()[0]})
                
                # Report progress.
                print('  Batch {:>5,}  of  {:>5,}.    Elapsed: {:}.'.format(step, len(train_dataloader), elapsed))


            b_input_ids = batch[0].to(device)
            b_input_mask = batch[1].to(device)
            b_labels = batch[2].to(device)

            # Always clear any previously calculated gradients before performing a
            # backward pass. PyTorch doesn't do this automatically because 
            # accumulating the gradients is "convenient while training RNNs". 
            # (source: https://stackoverflow.com/questions/48001598/why-do-we-need-to-call-zero-grad-in-pytorch)
            model.zero_grad()        

            loss, logits = model(b_input_ids, 
                                token_type_ids=None, 
                                attention_mask=b_input_mask, 
                                labels=b_labels)

              
            total_train_loss += loss.item()
            # total_train_loss += loss.cpu().detach().item()
            
            # Perform a backward pass to calculate the gradients.
            loss.backward()

            torch.nn.utils.clip_grad_norm_(model.parameters(), 1.0)

            optimizer.step()

            lr = scheduler.get_last_lr()[0]

            wandb.log({'lr':lr})
            scheduler.step()


        # Calculate the average loss over all of the batches.
        avg_train_loss = total_train_loss / len(train_dataloader)            
        
        # Measure how long this epoch took.
        training_time = format_time(time.time() - t0)

        print("")
        print("Running Validation...")

        t0 = time.time()

        model.eval()

        # Tracking variables 
        total_eval_accuracy = 0
        total_eval_loss = 0
        nb_eval_steps = 0
        

        # Evaluate data for one epoch
        for batch in validation_dataloader:
            
            b_input_ids = batch[0].cuda()
            b_input_mask = batch[1].to(device)
            b_labels = batch[2].to(device)
            
            # Tell pytorch not to bother with constructing the compute graph during
            # the forward pass, since this is only needed for backprop (training).
            with torch.no_grad():        


                (loss, logits) = model(b_input_ids, 
                                      token_type_ids=None, 
                                      attention_mask=b_input_mask,
                                      labels=b_labels)
                
            # Accumulate the validation loss.
            total_eval_loss += loss.item()

            # Move logits and labels to CPU
            logits = logits.detach().cpu().numpy()
            label_ids = b_labels.to('cpu').numpy()

            # Calculate the accuracy for this batch of test sentences, and
            # accumulate it over all batches.
            total_eval_accuracy += flat_accuracy(logits, label_ids)
            

        # Report the final accuracy for this validation run.
        avg_val_accuracy = total_eval_accuracy / len(validation_dataloader)
        print("  Accuracy: {0:.2f}".format(avg_val_accuracy))

        # Calculate the average loss over all of the batches.
        avg_val_loss = total_eval_loss / len(validation_dataloader)
        
        # Measure how long the validation run took.
        validation_time = format_time(time.time() - t0)
        wandb.log({'val_accuracy':avg_val_accuracy,'avg_val_loss':avg_val_loss})
        print("  Validation Loss: {0:.2f}".format(avg_val_loss))
        print("  Validation took: {:}".format(validation_time))
        
        # wandb.config.update({"epochs": 4, "batch_size": 32,"learning_rate": 5e-5,"best_val_accuracy": },allow_val_change=True)
        if epoch_i > epochs/5:
            with open(best_model_path +'/best_result.json') as json_file:
                data = json.load(json_file)
                for p in data:
                    bsresult = p['avg_val_accuracy']
                    print("from jsonfile bsresult",bsresult)
            
            if  avg_val_accuracy.item() > bsresult:
                print("Save BEST!! epoch_i",epoch_i,"was",bsresult,"now",avg_val_accuracy)
                # wandb.config.update({"epochs": 4, "batch_size": 32,"learning_rate": 5e-5,"best_val_accuracy": avg_val_accuracy.item()},allow_val_change=True)
                # wandb.config.update({"best_val_accuracy": avg_val_accuracy.item()},allow_val_change=True)            
                model.save_pretrained(best_model_path)
        
                data = []
                data.append({"epoch":epoch_i,"avg_val_accuracy":avg_val_accuracy.item(),"batch_size":wandb.config.batch_size,"learning_rate":wandb.config.learning_rate})
                with open(best_model_path +'/best_result.json', 'w') as outfile:
                    json.dump(data, outfile)

        wandb.log({'avg_train_loss':avg_train_loss})
        # wandb.log({"epochs":epoch_i,"avg_train_loss":avg_train_loss,"avg_val_loss": avg_val_loss,"avg_val_accuracy":avg_val_accuracy})

        # print("")
        print("  Average training loss: {0:.2f}".format(avg_train_loss))
        print("  Training epoch took: {:}".format(training_time))
        # wandb.save(best_model_path+'/model.h5')
            

    print("")
    print("Training complete!")

    print("Total training took {:} (h:mm:ss)".format(format_time(time.time()-total_t0)))
    # return model


# wandb.agent(sweep_id,function=train,count=5)

# wandb.init(config=sweep_defaults)
wandb.agent(sweep_id,function=train)

# production
# wandb.init(project='bertwandbprod',config={"epochs": 10, "batch_size": 128,"learning_rate": 5e-5,"best_val_accuracy": 0.0})
# train()
# wandb.save(best_model_path +'/*')


def classify(model,tokenizer,phrase):

   with torch.no_grad():
        encoded_dict = tokenizer.encode_plus(
                phrase,                      #6
                max_length = 64,           # Pad & truncate all sentences.
                pad_to_max_length = True,
                return_attention_mask = True,   # Construct attn. masks.
                return_tensors = 'pt',     # Return pytorch tensors.
            )
        input_ids = encoded_dict['input_ids'].cuda()
        attention_masks = encoded_dict['attention_mask'].cuda()
        res= model(input_ids,token_type_ids=None,attention_mask=attention_masks)
        top1_prob,top1label = torch.topk(res[0],1)
 
        pred = top1label[0].item()  
        softmaxlabel = softmax(res[0].detach().cpu())

        # print(softmaxlabel)
        # print(softmaxlabel[0][pred])
        accuracy = softmaxlabel[0][pred]
        return pred,accuracy

# wandbrundir = wandb.run.dir
# print("dir",wandbrundir)

# arr = os.listdir(wandbrundir)
# print(arr)
# arr =  os.listdir('/kaggle/working/wandb')
# print(arr)


if path.exists(best_model_path):

    wmodel = BertForSequenceClassification.from_pretrained(
            # "bert-base-uncased",
            best_model_path,
            # num_labels = 65, 
            # output_attentions = False, # Whether the model returns attentions weights.
            # output_hidden_states = False, # Whether the model returns all hidden-states.
        ).cuda()

    notgood = 0
    good = 0
    arr_score = []
    with open(datapath+'val.csv', newline='') as csvfile:
        reader = csv.DictReader(csvfile)
        for row in reader:
            artext = row['ask']
            labels = row['intent']
            pred,accuracy  = classify(wmodel,tokenizer,artext)
            # print(artext,pred,accuracy)
            if pred != int(labels):
                print("Not good", pred, accuracy)
                notgood = notgood+1
            else:
                arr_score.append(accuracy)
                good = good + 1

        numscore = np.array(arr_score, dtype=np.float32)

        print("scoreMIN", numscore.min(), "scoreMAX",numscore.max(), "scoreMIAN", numscore.mean())

        print("notgood",notgood,"good",good)
        print("not vs good", notgood/good)

else:
    print("NOT EXIST!!",best_model_dir)        


